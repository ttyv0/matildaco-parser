package matildacoParser

import "net/http"

type (
	LoginStruct struct {
		client  *http.Client
		siteUrl string
	}

	Thread struct {
		Id, Title, Author string
		Posts             []*Post
		login             LoginStruct
		exist             bool
		err               error
	}

	Post struct {
		Id       string
		Author   string
		AuthorId string
		Message  string
		Datetime string
	}
)
