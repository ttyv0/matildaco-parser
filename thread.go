package matildacoParser

import (
	"errors"
	"net/http"
	"net/url"
	"strconv"
	"strings"

	"github.com/PuerkitoBio/goquery"
)

func (l LoginStruct) NewThread(title, message string) *Thread {
	csrf, err := getCSRF(l.siteUrl+"/thread/create", l.client)
	if err != nil {
		return &Thread{login: l, Title: title, exist: false, err: err}
	}
	newPostVal := make(url.Values)
	newPostVal.Add("title", title)
	newPostVal.Add("message", message)
	newPostVal.Add("csrf_token", csrf)
	resp, err := l.client.PostForm(l.siteUrl+"/thread/create", newPostVal)
	defer resp.Body.Close()
	if err != nil {
		return &Thread{login: l, Title: title, exist: false, err: err}
	}
	return &Thread{login: l, Title: title, exist: true}
}

func (l LoginStruct) ThreadId(idInterface interface{}) *Thread {
	var id string
	switch idInterface := idInterface.(type) {
	case int:
		id = strconv.Itoa(idInterface)
	case string:
		id = idInterface
	default:
		panic(errors.New("ThreadId: Id is incorrect."))
	}
	resp, err := http.Get(l.siteUrl + "/thread/" + id)
	if err != nil || resp.StatusCode != 200 {
		return &Thread{login: l, exist: false, err: err}
	}
	doc, _ := goquery.NewDocumentFromResponse(resp)
	title := doc.Find(".lead").Text()
	return &Thread{login: l, Id: id, Title: title, exist: true}
}

func (l LoginStruct) ThreadTitle(str string) *Thread {
	doc, err := goquery.NewDocument(l.siteUrl)
	if err != nil {
		return nil
	}
	var threadId, title string
	doc.Find("tbody tr").EachWithBreak(func(i int, s *goquery.Selection) bool {
		a := s.Find("td h4 a")
		href, _ := a.Attr("href")
		title = a.Text()
		if strings.Contains(strings.ToLower(title), strings.ToLower(str)) {
			threadId = strings.TrimLeft(href, "/thread/")
			return false
		}
		return true
	})
	if threadId == "" {
		return &Thread{login: l, Title: str, exist: false}
	}
	return &Thread{login: l, Id: threadId, Title: title, exist: true}
}

func (t *Thread) parsePosts() error {
	if !t.exist {
		return errors.New("Thread incorrect")
	}
	doc, err := goquery.NewDocument(t.login.siteUrl + "/thread/" + t.Id)
	if err != nil {
		return err
	}
	pagen, b := doc.Find("a.me.next").Attr("href")
	var n int
	if b {
		n, _ = strconv.Atoi(strings.TrimPrefix(pagen, "/thread/"+t.Id+"/page/"))
	} else {
		n = 1
	}
	t.Posts = make([]*Post, 0)
	for i := 1; i <= n; i++ {
		doc, err := goquery.NewDocument(t.login.siteUrl + "/thread/" + t.Id + "/page/" + strconv.Itoa(i))
		if err != nil {
			return err
		}
		doc.Find("ul.media-list.forum li").Each(func(_ int, sel *goquery.Selection) {
			post := new(Post)
			post.Id, _ = sel.Attr("data-post")
			post.AuthorId, _ = sel.Attr("data-user")
			post.Author = sel.Find("strong").Text()
			post.Datetime = sel.Find("div.media-body a.btn.btn-default").Text()
			post.Message = sel.Find("div.media-body > p").Text()
			t.Posts = append(t.Posts, post)
		})
	}
	return nil
}
