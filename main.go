package matildacoParser

import (
	"errors"
	"net/http"
	"net/http/cookiejar"
	"net/url"

	"github.com/PuerkitoBio/goquery"
)

func Login(siteUrl, username, password string) (*LoginStruct, error) {
	cookieJar, _ := cookiejar.New(nil)
	client := &http.Client{Jar: cookieJar}
	csrf, err := getCSRF(siteUrl+"/login", client)
	if err != nil {
		return nil, err
	}
	loginFormVal := make(url.Values)
	loginFormVal.Add("username", username)
	loginFormVal.Add("password", password)
	loginFormVal.Add("csrf_token", csrf)
	resp, err := client.PostForm(siteUrl+"/login", loginFormVal)
	defer resp.Body.Close()
	if err != nil {
		return nil, err
	}
	if !checkLogin(resp) {
		return nil, errors.New("Check login error")
	}
	return &LoginStruct{client, siteUrl}, nil
}

func Registration(siteUrl, username, password, email string) (*LoginStruct, error) {
	cookieJar, _ := cookiejar.New(nil)
	client := &http.Client{Jar: cookieJar}
	csrf, err := getCSRF(siteUrl+"/register", client)
	if err != nil {
		return nil, err
	}
	regFormVal := make(url.Values)
	regFormVal.Add("username", username)
	regFormVal.Add("email", email)
	regFormVal.Add("password", password)
	regFormVal.Add("confirm", password)
	regFormVal.Add("accept_tos", "on")
	regFormVal.Add("csrf_token", csrf)
	resp, err := client.PostForm(siteUrl+"/register", regFormVal)
	if err != nil {
		return nil, err
	}
	if !checkLogin(resp) {
		return nil, errors.New("some error")
	}
	return &LoginStruct{client, siteUrl}, nil
}

func getCSRF(urlSite string, client *http.Client) (string, error) {
	resp, err := client.Get(urlSite)
	doc, _ := goquery.NewDocumentFromResponse(resp)
	defer resp.Body.Close()
	if err != nil {
		return "", err
	}
	csrf, _ := doc.Find("#csrf_token").Attr("value")
	if csrf == "" {
		return "", errors.New("csrf_token empty")
	}
	return csrf, nil
}

func checkLogin(r *http.Response) bool {
	doc, err := goquery.NewDocumentFromResponse(r)
	if err != nil {
		return false
	}
	link, _ := doc.Find("ul.navbar-right > :last-child > a").Attr("href")
	if link == "/logout" {
		return true
	}
	return false
}
