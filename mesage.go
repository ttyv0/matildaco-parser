package matildacoParser

import (
	"errors"
	"net/url"
)

func (t Thread) NewMessage(message string) error {
	if !t.exist {
		return errors.New("Thread incorrect")
	}
	csrf, err := getCSRF(t.login.siteUrl+"/thread/"+t.Id+"/page/1", t.login.client)
	if err != nil {
		return err
	}
	newMessageVal := make(url.Values)
	newMessageVal.Add("message", message)
	newMessageVal.Add("csrf_token", csrf)
	resp, err := t.login.client.PostForm(t.login.siteUrl+"/thread/"+t.Id+"/page/1", newMessageVal)
	defer resp.Body.Close()
	if err != nil {
		return err
	}
	return nil
}

func (t Thread) NewMessageForce(message string) error {
	if t.exist {
		return t.NewMessage(message)
	}
	return t.login.NewThread(t.Title, message).err
}
