package matildacoParser

import "net/url"

func (t *Thread) Vote(f func(*Post) bool, v string) error {
	if len(t.Posts) == 0 {
		err := t.parsePosts()
		if err != nil {
			return err
		}
	}
	for _, p := range t.Posts {
		if f(p) {
			csrf, err := getCSRF(t.login.siteUrl+"/thread/"+t.Id, t.login.client)
			if err != nil {
				return err
			}
			voteVal := make(url.Values)
			voteVal.Add("vote", v)
			voteVal.Add("post_id", p.Id)
			voteVal.Add("csrf_token", csrf)
			resp, err := t.login.client.PostForm(t.login.siteUrl+"/vote", voteVal)
			defer resp.Body.Close()
			if err != nil {
				return err
			}
		}
	}
	return nil
}

func PostsOfTheAuthor(a string) func(*Post) bool {
	return func(p *Post) bool {
		if p.Author == a {
			return true
		}
		return false
	}
}

func PostsOfTheAuthorId(id string) func(*Post) bool {
	return func(p *Post) bool {
		if p.AuthorId == id {
			return true
		}
		return false
	}
}

func AllPosts() func(*Post) bool {
	return func(_ *Post) bool {
		return true
	}
}
